/**
  * Copyright (c) 2010  Miriam Ruiz <miriam@ðebian.org>
  * Copyright (c) 2001 - 2005  Yuan Wang. All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without
  * modification, are permitted provided that the following conditions
  * are met:
  * 1. Redistributions of source code must retain the above copyright 
  * notice, this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright
  * notice, this list of conditions and the following disclaimer in the 
  * documentation and/or other materials provided with the distribution.
  * 3. Redistributions in any form must be accompanied by information on
  * how to obtain complete source code for the X-Diff software and any
  * accompanying software that uses the X-Diff software.  The source code
  * must either be included in the distribution or be available for no
  * more than the cost of distribution plus a nominal fee, and must be
  * freely redistributable under reasonable conditions.  For an executable
  * file, complete source code means the source code for all modules it
  * contains.  It does not include source code for modules or files that
  * typically accompany the major components of the operating system on
  * which the executable file runs.
  *
  * THIS SOFTWARE IS PROVIDED BY YUAN WANG "AS IS" AND ANY EXPRESS OR IMPLIED
  * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT,
  * ARE DISCLAIMED.  IN NO EVENT SHALL YUAN WANG BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
  * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  * POSSIBILITY OF SUCH DAMAGE.
  *
  */

#include "XDiff.hpp"
#include "XUtf8.hpp"

#include <gtk/gtk.h>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <cstdarg>

using namespace xercesc;

using std::vector;
using std::string;
using std::ofstream;
using std::ifstream;
using std::ios;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

enum {
	COL_DATA,
	COL_TOTAL
};

class FormatStrings {
public:
	// See: http://www.gtk.org/api/2.6/pango/PangoMarkupFormat.html
	static const char *BeginOpenTag() { return  "&lt;<b>%s</b>"; }
	static const char *EndOpenTag() { return  "&gt;" ; }
	static const char *CloseTag() { return  "&lt;/%s&gt;"; }
	static const char *OpenCloseTag() { return  "&lt;<b>%s</b>/&gt;"; }
	static const char *InsertNode() { return  "<span background='green'>%s</span>"; }
	static const char *DeleteNode() { return  "<span background='red'><s>%s</s></span>"; }
	static const char *AtagValue() { return  " %s=\"%s\""; }
	static const char *AtagValueInserted() { return  " <span background='green'>%s=\"%s\"</span>"; }
	static const char *AtagValueDeleted() { return  " <span background='red'><s>%s=\"%s\"</s></span>"; }
	static bool ShowCloseTag() { return false; }
};

static const string USAGE = "xdiff [-o|-g] [-p percent] input_xml1 input_xml2\nOptions:\n  The default mode is \"-o -p 0.3\"\n  -o\tThe optimal mode, to get the minimum editing distance.\n  -g\tThe greedy mode, to find a difference quickly.\n  -p\tThe maximum change percentage allowed.\n\tDefault value: 1.0 for -o mode; 0.3 for -g mode.";

static double DiffTime(const struct timeval *time1, const struct timeval *time2)
{
	long sec = time2->tv_sec - time1->tv_sec;
	long usec = time2->tv_usec - time1->tv_usec;
	if (usec < 0)
	{
		sec--;
		usec += 1000000;
	}
	return 0.000001 * usec + sec;
}

// Thanks to Larry Gritz for this code
// See: http://stackoverflow.com/questions/69738/c-how-to-get-fprintf-results-as-a-stdstring-w-o-sprintf
static std::string string_vformat(const char *fmt, va_list ap)
{
	// Allocate a buffer on the stack that's big enough for us almost
	// all the time.  Be prepared to allocate dynamically if it doesn't fit.
	size_t size = 1024;
	char stackbuf[1024];
	std::vector<char> dynamicbuf;
	char *buf = &stackbuf[0];

	while (1) {
		// Try to vsnprintf into our buffer.
		int needed = vsnprintf (buf, size, fmt, ap);
		// NB. C99 (which modern Linux and OS X follow) says vsnprintf
		// failure returns the length it would have needed.  But older
		// glibc and current Windows return -1 for failure, i.e., not
		// telling us how much was needed.

		if (needed <= (int)size && needed >= 0) {
			// It fit fine so we're done.
			return std::string (buf, (size_t) needed);
		}

		// vsnprintf reported that it wanted to write more characters
		// than we allotted.  So try again using a dynamic buffer.  This
		// doesn't happen very often if we chose our initial size well.
		size = (needed > 0) ? (needed+1) : (size*2);
		dynamicbuf.resize (size);
		buf = &dynamicbuf[0];
	}
}

static std::string string_printf(const char *fmt, ...)
{
	va_list ap;
	va_start (ap, fmt);
	std::string buf = string_vformat(fmt, ap);
	va_end (ap);
	return buf;
}

static string ConstructText(XTree *xtree, int eid)
{
	xtree->dump(eid);
	string text = xtree->getText(eid);
	vector<size_t> cdatalist = xtree->getCDATA(eid);
	if (cdatalist.empty())
		return string("T: ") + text;

	string buf = "";
	int count = cdatalist.size();
	size_t lastEnd = 0;
	for (int i = 0; i < count; i += 2)
	{
		size_t cdataStart = cdatalist[i];
		size_t cdataEnd = cdatalist[i+1];

		if (cdataStart > lastEnd)
			buf += text.substr(lastEnd, cdataStart - lastEnd);
		buf += "<![CDATA[" +
			text.substr(cdataStart, cdataEnd - cdataStart) +
			"]]>";
		lastEnd = cdataEnd;
	}

	if (lastEnd < text.length())
		buf += text.substr(lastEnd);

	return string("B: ") + buf;
}

static void SetFilteredTreeLine(GtkTreeStore *pTreeModel, GtkTreeIter *pTreeItem, const char *text, const char *format = "%s")
{
	// Valid formats: http://www.gtk.org/api/2.6/pango/PangoMarkupFormat.html
	std::ostringstream out;
	utf8::escape_text(out, text);
	out << std::ends;
	out.flush();
	const std::string &tmpStr(out.str());
	gtk_tree_store_set (pTreeModel, pTreeItem, COL_DATA, string_printf(format, tmpStr.c_str()).c_str(), -1);
}

static void SetTreeLine(GtkTreeStore *pTreeModel, GtkTreeIter *pTreeItem, const char *text, const char *format = "%s")
{
	// Valid formats: http://www.gtk.org/api/2.6/pango/PangoMarkupFormat.html
	gtk_tree_store_set (pTreeModel, pTreeItem, COL_DATA, string_printf(format, text).c_str(), -1);
}

static void ShowMatchNode(GtkTreeStore *pTreeModel, GtkTreeIter *pTreeTop, XTree *xtree, int node)
{
	std::ostringstream out;
	GtkTreeIter tree_child;
	gtk_tree_store_append (pTreeModel, &tree_child, pTreeTop);
	gtk_tree_store_set (pTreeModel, &tree_child, COL_DATA, "** Match Node **", -1);

	if (xtree->isElement(node))
	{
		string tag = xtree->getTag(node);
		out << string_printf(FormatStrings::BeginOpenTag(), tag.c_str());

		// Attributes.
		int attr = xtree->getFirstAttribute(node);
		while (attr > 0)
		{
			string atag = xtree->getTag(attr);
			string value = xtree->getAttributeValue(attr);
			out << string_printf(FormatStrings::AtagValue(), atag.c_str(), value.c_str());
			attr = xtree->getNextAttribute(attr);
		}

		// Child nodes.
		int child = xtree->getFirstChild(node);
		if (child < 0)
		{
			out << string_printf(FormatStrings::OpenCloseTag(), tag.c_str());
			return;
		}

		out << FormatStrings::EndOpenTag();

		while (child > 0)
		{
			ShowMatchNode(pTreeModel, &tree_child, xtree, child);
			child = xtree->getNextSibling(child);
		}

		if (FormatStrings::ShowCloseTag())
		{
			GtkTreeIter tree_child_close;
			gtk_tree_store_append (pTreeModel, &tree_child_close, pTreeTop);
			gtk_tree_store_set (pTreeModel, &tree_child_close, COL_DATA,
				string_printf(FormatStrings::CloseTag(), tag.c_str()).c_str(), -1);
		}
	}
	else
	{
		out << ConstructText(xtree, node);
	}
	out << std::ends;
	out.flush();
	const std::string &tmpStr(out.str());
	SetTreeLine(pTreeModel, &tree_child, tmpStr.c_str());
}

static void ShowDeleteNode(const XDiff &xdiff, GtkTreeStore *pTreeModel, GtkTreeIter *pTreeTop, int node)
{
	std::ostringstream out;
	GtkTreeIter tree_child;
	gtk_tree_store_append (pTreeModel, &tree_child, pTreeTop);
	gtk_tree_store_set (pTreeModel, &tree_child, COL_DATA, "** Delete Node **", -1);

	XTree *xtree1 = xdiff.GetTree1();
	if (xtree1->isElement(node))
	{
		string tag = xtree1->getTag(node);
		out << string_printf(FormatStrings::BeginOpenTag(), tag.c_str());

		// Attributes.
		int attr = xtree1->getFirstAttribute(node);
		while (attr > 0)
		{
			string atag = xtree1->getTag(attr);
			string value = xtree1->getAttributeValue(attr);
			out << string_printf(FormatStrings::AtagValue(), atag.c_str(), value.c_str());
			attr = xtree1->getNextAttribute(attr);
		}

		// Child nodes.
		int child = xtree1->getFirstChild(node);

		if (child < 0)
		{
			out << "/" << FormatStrings::EndOpenTag();
			out << "<?DELETE " << tag << "?>";
			return;
		}

		out << FormatStrings::EndOpenTag();
		out << "<?DELETE " << tag << "?>";

		while (child > 0)
		{
			ShowMatchNode(pTreeModel, &tree_child, xtree1, child);
			child = xtree1->getNextSibling(child);
		}

		if (FormatStrings::ShowCloseTag())
		{
			GtkTreeIter tree_child_close;
			gtk_tree_store_append (pTreeModel, &tree_child_close, pTreeTop);
			gtk_tree_store_set (pTreeModel, &tree_child_close, COL_DATA,
				string_printf(FormatStrings::CloseTag(), tag.c_str()).c_str(), -1);
		}
	}
	else
	{
		out << "<?DELETE \"" << ConstructText(xtree1, node) << "\"?>";
	}
	out << std::ends;
	out.flush();
	const std::string &tmpStr(out.str());
	SetTreeLine(pTreeModel, &tree_child, tmpStr.c_str(), FormatStrings::DeleteNode());
}

static void ShowInsertNode(const XDiff &xdiff, GtkTreeStore *pTreeModel, GtkTreeIter *pTreeTop, int node)
{
	std::ostringstream out;
	GtkTreeIter tree_child;
	gtk_tree_store_append (pTreeModel, &tree_child, pTreeTop);
	gtk_tree_store_set (pTreeModel, &tree_child, COL_DATA, "** Insert Node **", -1);

	XTree *xtree2 = xdiff.GetTree2();
	if (xtree2->isElement(node))
	{
		string tag = xtree2->getTag(node);
		out << string_printf(FormatStrings::BeginOpenTag(), tag.c_str());

		// Attributes.
		int attr = xtree2->getFirstAttribute(node);
		while (attr > 0)
		{
			string atag = xtree2->getTag(attr);
			string value = xtree2->getAttributeValue(attr);
			out << string_printf(FormatStrings::AtagValue(), atag.c_str(), value.c_str());
			attr = xtree2->getNextAttribute(attr);
		}

		// Child nodes.
		int child = xtree2->getFirstChild(node);
		if (child < 0)
		{
			out << "/" << FormatStrings::EndOpenTag();
			out << "<?INSERT " << tag << "?>";
			return;
		}

		out << FormatStrings::EndOpenTag();
		out << "<?INSERT " << tag << "?>";

		while (child > 0)
		{
			ShowMatchNode(pTreeModel, &tree_child, xtree2, child);
			child = xtree2->getNextSibling(child);
		}

		if (FormatStrings::ShowCloseTag())
		{
			GtkTreeIter tree_child_close;
			gtk_tree_store_append (pTreeModel, &tree_child_close, pTreeTop);
			gtk_tree_store_set (pTreeModel, &tree_child_close, COL_DATA,
				string_printf(FormatStrings::CloseTag(), tag.c_str()).c_str(), -1);
		}
	}
	else
	{
		out << ConstructText(xtree2, node) << "<?INSERT?>";
	}
	out << std::ends;
	out.flush();
	const std::string &tmpStr(out.str());
	SetTreeLine(pTreeModel, &tree_child, tmpStr.c_str(), FormatStrings::InsertNode());
}

static void ShowDiffNode(const XDiff &xdiff, GtkTreeStore *pTreeModel, GtkTreeIter *pTreeTop, int node1, int node2)
{
	std::ostringstream out;
	GtkTreeIter tree_child;
	gtk_tree_store_append (pTreeModel, &tree_child, pTreeTop);
	gtk_tree_store_set (pTreeModel, &tree_child, COL_DATA, "** Diff Node **", -1);

	XTree *xtree1 = xdiff.GetTree1();
	XTree *xtree2 = xdiff.GetTree2();

	if (xtree1->isElement(node1))
	{
		string tag = xtree1->getTag(node1);
		out << string_printf(FormatStrings::BeginOpenTag(), tag.c_str());

		// Attributes.
		int attr1 = xtree1->getFirstAttribute(node1);
		string diffff = "";
		while (attr1 > 0)
		{
			string atag = xtree1->getTag(attr1);
			string value = xtree1->getAttributeValue(attr1);
			int matchType, matchNode;
			xtree1->getMatching(attr1, matchType, matchNode);
			if (matchType == XTree::MATCH)
			{
				out << string_printf(FormatStrings::AtagValue(), atag.c_str(), value.c_str());
			}
			else if (matchType == XTree::DELETE)
			{
				out << string_printf(FormatStrings::AtagValueDeleted(), atag.c_str(), value.c_str());
			}
			else
			{
				out << string_printf(FormatStrings::AtagValueDeleted(), atag.c_str(), value.c_str());
				string value2 = xtree2->getAttributeValue(matchNode);
				out << string_printf(FormatStrings::AtagValueInserted(), atag.c_str(), value2.c_str());
			}

			attr1 = xtree1->getNextAttribute(attr1);
		}

		int attr2 = xtree2->getFirstAttribute(node2);
		while (attr2 > 0)
		{
			int matchType, matchNode;
			xtree2->getMatching(attr2, matchType, matchNode);
			if (matchType == XTree::INSERT)
			{
				string atag = xtree2->getTag(attr2);
				string value = xtree2->getAttributeValue(attr2);
				out << string_printf(FormatStrings::AtagValueInserted(), atag.c_str(), value.c_str());
			}

			attr2 = xtree2->getNextAttribute(attr2);
		}

		// Child nodes.
		int child1 = xtree1->getFirstChild(node1);
		if (child1 < 0)
		{
			out << "/" << FormatStrings::EndOpenTag() << diffff;
			return;
		}

		out << FormatStrings::EndOpenTag() << diffff;

		while (child1 > 0)
		{
			int matchType, matchNode;
			xtree1->getMatching(child1, matchType, matchNode);
			if (matchType == XTree::MATCH)
				ShowMatchNode(pTreeModel, &tree_child, xtree1, child1);
			else if (matchType == XTree::DELETE)
				ShowDeleteNode(xdiff, pTreeModel, &tree_child, child1);
			else
				ShowDiffNode(xdiff, pTreeModel, &tree_child, child1, matchNode);

			child1 = xtree1->getNextSibling(child1);
		}

		int child2 = xtree2->getFirstChild(node2);
		while (child2 > 0)
		{
			int matchType, matchNode;
			xtree2->getMatching(child2, matchType, matchNode);
			if (matchType == XTree::INSERT)
				ShowInsertNode(xdiff, pTreeModel, &tree_child, child2);

			child2 = xtree2->getNextSibling(child2);
		}

		if (FormatStrings::ShowCloseTag())
		{
			GtkTreeIter tree_child_close;
			gtk_tree_store_append (pTreeModel, &tree_child_close, pTreeTop);
			gtk_tree_store_set (pTreeModel, &tree_child_close, COL_DATA,
				string_printf(FormatStrings::CloseTag(), tag.c_str()).c_str(), -1);
		}
	}
	else
	{
		out << ConstructText(xtree2, node2) << "<?UPDATE FROM \"" 
			<< ConstructText(xtree1, node1) << "\"?>";
	}
	out << std::ends;
	out.flush();
	const std::string &tmpStr(out.str());
	SetTreeLine(pTreeModel, &tree_child, tmpStr.c_str());
}

static void ShowDifference(const XDiff &xdiff, GtkTreeStore *pTreeModel, GtkTreeIter *pTreeTop)
{
	struct timeval	*tv0 = new struct timeval;
	struct timeval	*tv1 = new struct timeval;
	struct timezone	*tz = new struct timezone;

	// after diffing.
	gettimeofday(tv0, tz);

//	xdiff.GetTree1()->dump();
//	xdiff.GetTree2()->dump();

	int root1 = xdiff.GetTree1()->getRoot();
	int root2 = xdiff.GetTree2()->getRoot();
	int matchType, matchNode;
	xdiff.GetTree1()->getMatching(root1, matchType, matchNode);
	if (matchType == XTree::DELETE)
	{
		ShowDeleteNode(xdiff, pTreeModel, pTreeTop, root1);
		ShowInsertNode(xdiff, pTreeModel, pTreeTop, root2);
	}
	else
	{
		ShowDiffNode(xdiff, pTreeModel, pTreeTop, root1, root2);
	}

	gettimeofday(tv1, tz);

	cerr << "Showing diff:\t\t" << DiffTime(tv0, tv1) << " s\n";

	delete tv0;
	delete tv1;
	delete tz;
}

/*
 * For the GTK+ code used as a base:
 *
 * Copyright (C) 2000-2007, Lee Thomason <leethomason@mindspring.com>
 * Copyright (C) 2002-2004, Yves Berquin <yvesb@users.sourceforge.net>
 * 
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any
 * damages arising from the use of this software.
 * 
 * Permission is granted to anyone to use this software for any
 * purpose, including commercial applications, and to alter it and
 * redistribute it freely, subject to the following restrictions:
 * 
 * 1. The origin of this software must not be misrepresented; you must
 * not claim that you wrote the original software. If you use this
 * software in a product, an acknowledgment in the product documentation
 * would be appreciated but is not required.
 * 
 * 2. Altered source versions must be plainly marked as such, and
 * must not be misrepresented as being the original software.
 * 
 * 3. This notice may not be removed or altered from any source
 * distribution.
 */

static GtkTreeModel *create_model(const XDiff &xdiff)
{
	GtkTreeStore *model;
	model = gtk_tree_store_new (COL_TOTAL, G_TYPE_STRING);
	ShowDifference(xdiff, model, NULL);
	return GTK_TREE_MODEL (model);
}

static GtkWidget *create_view_and_model(const XDiff &xdiff)
{
	GtkWidget *view;
	GtkTreeModel *model;
	GtkTreeViewColumn *col;
	GtkCellRenderer *renderer;
	view = gtk_tree_view_new();
	col = gtk_tree_view_column_new();
	gtk_tree_view_column_set_title(col, "XML Tag");
	gtk_tree_view_append_column(GTK_TREE_VIEW (view), col);
	renderer = gtk_cell_renderer_text_new();
	gtk_tree_view_column_pack_start(col, renderer, TRUE);
//	gtk_tree_view_column_add_attribute(col, renderer, "text", COL_DATA);
	gtk_tree_view_column_add_attribute(col, renderer, "markup", COL_DATA);
	model = create_model(xdiff);
	gtk_tree_view_set_model(GTK_TREE_VIEW (view), model);
	g_object_unref(model);
	return view;
}

static gboolean app_quit(GtkWidget *widget, GdkEvent *event, gpointer data)
{
	gtk_main_quit();
	return TRUE;
}

int main(int argc, char* args[])
{
	bool	oFlag = false, gFlag = false;
	double	NoMatchThreshold = 0.3;

	// You can use a different number for sampling here,
	// as sqrt(n) is a safe one, though 3 works pretty well.
	int sample_count = 3;

	try
	{
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException& e)
	{
		cerr << "Error during initialization! :\n" << e.getMessage() << endl;
		exit(1);
	}

	int	opid = 1;
	if (argc < 3)
	{
		cerr << USAGE << endl;
		exit(1);
	}
	else if (strcmp(args[1], "-o") == 0)
	{
		oFlag = true;
		opid++;
	}
	else if (strcmp(args[1], "-g") == 0)
	{
		gFlag = true;
		opid++;
	}

	if (strcmp(args[opid], "-p") == 0)
	{
		opid++;
		double	p = strtod(args[opid], NULL);
		if ((p <= 0) || (p > 1))
		{
			cerr << USAGE << endl;
			exit(1);
		}
		NoMatchThreshold = p;
		opid++;
	}

	if ((argc - opid) != 2)
	{
		cerr << USAGE << endl;
		exit(1);
	}

	XDiff xdiff(args[opid], args[opid+1], oFlag, gFlag, NoMatchThreshold, sample_count);

	GtkWidget *window;
	GtkWidget *view;
	gtk_init(&argc, &args);
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	g_signal_connect(window, "delete_event", G_CALLBACK (app_quit), NULL);
	view = create_view_and_model(xdiff);
	gtk_container_add(GTK_CONTAINER (window), view);
	gtk_widget_show_all(window);
	gtk_main();

}
