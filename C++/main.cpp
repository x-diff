/**
  * Copyright (c) 2010  Miriam Ruiz <miriam@ðebian.org>
  * Copyright (c) 2001 - 2005  Yuan Wang. All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without
  * modification, are permitted provided that the following conditions
  * are met:
  * 1. Redistributions of source code must retain the above copyright 
  * notice, this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright
  * notice, this list of conditions and the following disclaimer in the 
  * documentation and/or other materials provided with the distribution.
  * 3. Redistributions in any form must be accompanied by information on
  * how to obtain complete source code for the X-Diff software and any
  * accompanying software that uses the X-Diff software.  The source code
  * must either be included in the distribution or be available for no
  * more than the cost of distribution plus a nominal fee, and must be
  * freely redistributable under reasonable conditions.  For an executable
  * file, complete source code means the source code for all modules it
  * contains.  It does not include source code for modules or files that
  * typically accompany the major components of the operating system on
  * which the executable file runs.
  *
  * THIS SOFTWARE IS PROVIDED BY YUAN WANG "AS IS" AND ANY EXPRESS OR IMPLIED
  * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT,
  * ARE DISCLAIMED.  IN NO EVENT SHALL YUAN WANG BE LIABLE FOR ANY DIRECT,
  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
  * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  * POSSIBILITY OF SUCH DAMAGE.
  *
  */

#include "XDiff.hpp"

using namespace xercesc;

using std::vector;
using std::string;
using std::ofstream;
using std::ifstream;
using std::ios;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

static const string USAGE = "xdiff [-o|-g] [-p percent] input_xml1 input_xml2 diff_result\nOptions:\n  The default mode is \"-o -p 0.3\"\n  -o\tThe optimal mode, to get the minimum editing distance.\n  -g\tThe greedy mode, to find a difference quickly.\n  -p\tThe maximum change percentage allowed.\n\tDefault value: 1.0 for -o mode; 0.3 for -g mode.";

static double DiffTime(const struct timeval *time1, const struct timeval *time2)
{
	long sec = time2->tv_sec - time1->tv_sec;
	long usec = time2->tv_usec - time1->tv_usec;
	if (usec < 0)
	{
		sec--;
		usec += 1000000;
	}
	return 0.000001 * usec + sec;
}

static string ConstructText(XTree *xtree, int eid)
{
	string text = xtree->getText(eid);
	vector<size_t> cdatalist = xtree->getCDATA(eid);
	if (cdatalist.empty())
		return text;

	string buf = "";
	int count = cdatalist.size();
	size_t lastEnd = 0;
	for (int i = 0; i < count; i += 2)
	{
		size_t cdataStart = cdatalist[i];
		size_t cdataEnd = cdatalist[i+1];

		if (cdataStart > lastEnd)
			buf += text.substr(lastEnd, cdataStart - lastEnd);
		buf += "<![CDATA[" +
			text.substr(cdataStart, cdataEnd - cdataStart) +
			"]]>";
		lastEnd = cdataEnd;
	}

	if (lastEnd < text.length())
		buf += text.substr(lastEnd);

	return buf;
}

static bool needNewLine;

static void WriteMatchNode(ofstream &out, XTree *xtree, int node)
{
	if (xtree->isElement(node))
	{
		string tag = xtree->getTag(node);
		if (needNewLine)
			out << endl;

		out << "<" << tag;

		// Attributes.
		int attr = xtree->getFirstAttribute(node);
		while (attr > 0)
		{
			string atag = xtree->getTag(attr);
			string value = xtree->getAttributeValue(attr);
			out << " " << atag << "=\"" << value << "\"";
			attr = xtree->getNextAttribute(attr);
		}

		// Child nodes.
		int child = xtree->getFirstChild(node);
		if (child < 0)
		{
			out << "/>" << endl;
			needNewLine = false;
			return;
		}

		out << ">";
		needNewLine = true;

		while (child > 0)
		{
			WriteMatchNode(out, xtree, child);
			child = xtree->getNextSibling(child);
		}

		if (needNewLine)
		{
			out << endl;
			needNewLine = false;
		}

		out << "</" << tag << ">" << endl;
	}
	else
	{
		out << ConstructText(xtree, node);
		needNewLine = false;
	}
}

static void WriteDeleteNode(const XDiff &xdiff, ofstream &out, int node)
{
	XTree *xtree1 = xdiff.GetTree1();
	if (xtree1->isElement(node))
	{
		string tag = xtree1->getTag(node);
		out << "<" << tag;

		// Attributes.
		int attr = xtree1->getFirstAttribute(node);
		while (attr > 0)
		{
			string atag = xtree1->getTag(attr);
			string value = xtree1->getAttributeValue(attr);
			out << " " << atag << "=\"" << value << "\"";
			attr = xtree1->getNextAttribute(attr);
		}

		// Child nodes.
		int child = xtree1->getFirstChild(node);

		if (child < 0)
		{
			out << "/><?DELETE " << tag << "?>" << endl;
			needNewLine = false;
			return;
		}

		out << "><?DELETE " << tag << "?>" << endl;
		needNewLine = false;

		while (child > 0)
		{
			WriteMatchNode(out, xtree1, child);
			child = xtree1->getNextSibling(child);
		}

		if (needNewLine)
		{
			out << endl;
			needNewLine = false;
		}

		out << "</" << tag << ">" << endl;
	}
	else
	{
		out << "<?DELETE \"" << ConstructText(xtree1, node)
			<< "\"?>" << endl;
		needNewLine = false;
	}
}

static void WriteInsertNode(const XDiff &xdiff, ofstream &out, int node)
{
	XTree *xtree2 = xdiff.GetTree2();
	if (xtree2->isElement(node))
	{
		string tag = xtree2->getTag(node);
		out << "<" << tag;

		// Attributes.
		int attr = xtree2->getFirstAttribute(node);
		while (attr > 0)
		{
			string atag = xtree2->getTag(attr);
			string value = xtree2->getAttributeValue(attr);
			out << " " << atag << "=\"" << value << "\"";
			attr = xtree2->getNextAttribute(attr);
		}

		// Child nodes.
		int child = xtree2->getFirstChild(node);
		if (child < 0)
		{
			out << "/><?INSERT " << tag << "?>" << endl;
			needNewLine = false;
			return;
		}

		out << "><?INSERT " << tag << "?>" << endl;
		needNewLine = false;

		while (child > 0)
		{
			WriteMatchNode(out, xtree2, child);
			child = xtree2->getNextSibling(child);
		}

		if (needNewLine)
		{
			out << endl;
			needNewLine = false;
		}

		out << "</" << tag << ">" << endl;
	}
	else
	{
		out << ConstructText(xtree2, node) << "<?INSERT?>" << endl;
		needNewLine = false;
	}
}

static void WriteDiffNode(const XDiff &xdiff, ofstream &out, int node1, int node2)
{
	XTree *xtree1 = xdiff.GetTree1();
	XTree *xtree2 = xdiff.GetTree2();
	if (xtree1->isElement(node1))
	{
		string tag = xtree1->getTag(node1);
		if (needNewLine)
			out << endl;
		out << "<" << tag;

		// Attributes.
		int attr1 = xtree1->getFirstAttribute(node1);
		string diffff = "";
		while (attr1 > 0)
		{
			string atag = xtree1->getTag(attr1);
			string value = xtree1->getAttributeValue(attr1);
			int matchType, matchNode;
			xtree1->getMatching(attr1, matchType, matchNode);
			if (matchType == XTree::MATCH)
				out << " " << atag << "=\"" << value << "\"";
			else if (matchType == XTree::DELETE)
			{
				out << " " << atag << "=\"" << value << "\"";
				diffff += "<?DELETE " + atag + "?>";
			}
			else
			{
				string value2 = xtree2->getAttributeValue(matchNode);
				out << " " << atag << "=\"" << value2 << "\"";
				diffff += "<?UPDATE " + atag +
					  " FROM \"" + value + "\"?>";
			}

			attr1 = xtree1->getNextAttribute(attr1);
		}

		int attr2 = xtree2->getFirstAttribute(node2);
		while (attr2 > 0)
		{
			int matchType, matchNode;
			xtree2->getMatching(attr2, matchType, matchNode);
			if (matchType == XTree::INSERT)
			{
				string atag = xtree2->getTag(attr2);
				string value = xtree2->getAttributeValue(attr2);
				out << " " << atag << "=\"" << value << "\"";
				diffff += "<?INSERT " + atag + "?>";
			}

			attr2 = xtree2->getNextAttribute(attr2);
		}

		// Child nodes.
		int child1 = xtree1->getFirstChild(node1);
		if (child1 < 0)
		{
			out << "/>" << diffff << endl;
			needNewLine = false;
			return;
		}

		out << ">" << diffff;
		needNewLine = true;

		while (child1 > 0)
		{
			int matchType, matchNode;
			xtree1->getMatching(child1, matchType, matchNode);
			if (matchType == XTree::MATCH)
				WriteMatchNode(out, xtree1, child1);
			else if (matchType == XTree::DELETE)
				WriteDeleteNode(xdiff, out, child1);
			else
				WriteDiffNode(xdiff, out, child1, matchNode);

			child1 = xtree1->getNextSibling(child1);
		}

		int child2 = xtree2->getFirstChild(node2);
		while (child2 > 0)
		{
			int matchType, matchNode;
			xtree2->getMatching(child2, matchType, matchNode);
			if (matchType == XTree::INSERT)
				WriteInsertNode(xdiff, out, child2);

			child2 = xtree2->getNextSibling(child2);
		}

		if (needNewLine)
		{
			out << endl;
			needNewLine = false;
		}

		out << "</" << tag << ">" << endl;
	}
	else
	{
		out << ConstructText(xtree2, node2) << "<?UPDATE FROM \"" 
			<< ConstructText(xtree1, node1) << "\"?>";
		needNewLine = false;
	}
}

static void WriteDifference(const XDiff &xdiff, const char *output, const char *input)
{
	struct timeval *tv0 = new struct timeval;
	struct timeval *tv1 = new struct timeval;
	struct timezone *tz = new struct timezone;

	needNewLine = false;
	gettimeofday(tv0, tz);

	int root1 = xdiff.GetTree1()->getRoot();
	int root2 = xdiff.GetTree2()->getRoot();
	ofstream out(output, ios::out|ios::trunc);

	if (input) // the header
	{
		ifstream in(input);
		// XXX <root > is valid and should be treated as <root>;
		// < root> is NOT valid, so use <root as the comparison key.
		string rootTag = "<" + xdiff.GetTree1()->getTag(root1);
		string line;
		while (getline(in, line))
		{
			if (line.find(rootTag) != string::npos)
				break;
			out << line << endl;
		}
		in.close();
	}

	int matchType, matchNode;
	xdiff.GetTree1()->getMatching(root1, matchType, matchNode);
	if (matchType == XTree::DELETE)
	{
		WriteDeleteNode(xdiff, out, root1);
		WriteInsertNode(xdiff, out, root2);
	}
	else
	{
		WriteDiffNode(xdiff, out, root1, root2);
	}

	out.close();

	gettimeofday(tv1, tz);

	cerr << "Writing diff:\t\t" << DiffTime(tv0, tv1) << " s\n";

	delete tv0;
	delete tv1;
	delete tz;
}

int main(int argc, char* args[])
{
	bool	oFlag = false, gFlag = false;
	double	NoMatchThreshold = 0.3;

	// You can use a different number for sampling here,
	// as sqrt(n) is a safe one, though 3 works pretty well.
	int sample_count = 3;

	try
	{
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException& e)
	{
		cerr << "Error during initialization! :\n"
			<< e.getMessage() << endl;
		exit(1);
	}

	int	opid = 1;
	if (argc < 4)
	{
		cerr << USAGE << endl;
		exit(1);
	}
	else if (strcmp(args[1], "-o") == 0)
	{
		oFlag = true;
		opid++;
	}
	else if (strcmp(args[1], "-g") == 0)
	{
		gFlag = true;
		opid++;
	}

	if (strcmp(args[opid], "-p") == 0)
	{
		opid++;
		double	p = strtod(args[opid], NULL);
		if ((p <= 0) || (p > 1))
		{
			cerr << USAGE << endl;
			exit(1);
		}
		NoMatchThreshold = p;
		opid++;
	}

	if ((argc - opid) != 3)
	{
		cerr << USAGE << endl;
		exit(1);
	}

	XDiff xdiff(args[opid], args[opid+1], oFlag, gFlag, NoMatchThreshold, sample_count);
	WriteDifference(xdiff, args[opid+2], args[opid]);
}
